package com.spring.springboot.service;

import com.spring.springboot.dto.OrderDto;
import com.spring.springboot.mapper.OrderMapper;
import com.spring.springboot.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.util.List;


@Service
@RequiredArgsConstructor
@Log4j2
@Transactional
public class OrderService {

    private final OrderRepository repository;

    private final OrderMapper mapper;

    public List<OrderDto> getAll(){

        log.info("Get all orders");
        return mapper.toDto(repository.findAll());
    }

    public OrderDto getById(Long id){

        log.info("Getting order by ID {}", id);
        return mapper.toDto(repository.getReferenceById(id));
    }

    public OrderDto save(OrderDto order){

        order.setId(null);
        log.info("Creating order {}", order);
        return mapper.toDto(repository.save(mapper.toEntity(order)));
    }

    public OrderDto update(OrderDto order){

        log.info("Updating order {}", order);
        return mapper.toDto(repository.save(mapper.toEntity(order)));
    }

    public void delete(Long id){

        log.info("Deleting order by ID {}", id);

        repository.deleteById(id);
    }
}
