package com.spring.springboot.service;

import com.spring.springboot.dto.ProductDto;
import com.spring.springboot.mapper.ProductMapper;
import com.spring.springboot.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@RequiredArgsConstructor
@Log4j2
@Transactional
public class ProductService {

    private final ProductRepository repository;

    private final ProductMapper mapper;

    public List<ProductDto> getAll(){

        log.info("Get all products");
        return mapper.toDto(repository.findAll());
    }

    public ProductDto getById(Long id){

        log.info("Getting product by ID {}", id);
        return mapper.toDto(repository.getReferenceById(id));
    }

    public ProductDto save(ProductDto product){

        product.setId(null);
        log.info("Creating product {}", product);
        return mapper.toDto(repository.save(mapper.toEntity(product)));
    }

    public ProductDto update(ProductDto product){

        log.info("Updating product {}", product);
        return mapper.toDto(repository.save(mapper.toEntity(product)));
    }

    public void delete(Long id){

        log.info("Deleting product by ID {}", id);

        repository.deleteById(id);
    }
}
