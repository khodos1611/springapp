package com.spring.springboot.service;


import com.spring.springboot.dto.MyUserDto;
import com.spring.springboot.mapper.MyUserMapper;
import com.spring.springboot.repository.MyUserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.util.List;

@Service
@RequiredArgsConstructor
@Log4j2
@Transactional
public class MyUserService {

    private final MyUserRepository repository;
    private final MyUserMapper mapper;

    public MyUserDto getByName(String name){

        log.info("Getting user by name", name);
        return mapper.toDto(repository.findByName(name));
    }

    public List<MyUserDto> getAll(){

        log.info("Getting all users");
        return mapper.toDto(repository.findAll());
    }

}
