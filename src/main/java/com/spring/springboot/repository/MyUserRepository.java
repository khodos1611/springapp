package com.spring.springboot.repository;

import com.spring.springboot.entity.auth.MyUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MyUserRepository extends JpaRepository<MyUser, Long> {
    MyUser findByName(String name);
}
