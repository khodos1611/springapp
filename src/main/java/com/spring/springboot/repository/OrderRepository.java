package com.spring.springboot.repository;


import com.spring.springboot.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import java.sql.Date;
import java.util.List;

public interface OrderRepository extends JpaRepository<Order, Long> {
    List<Order> findByDate(Date date);
}
