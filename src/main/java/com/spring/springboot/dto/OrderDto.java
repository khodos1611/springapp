package com.spring.springboot.dto;

import lombok.Data;
import lombok.ToString;

import java.sql.Date;
import java.util.List;

@Data
@ToString
public class OrderDto {

    private Long id;
    private Date date;
    private double cost;
    private List<ProductDto> productList;

}
