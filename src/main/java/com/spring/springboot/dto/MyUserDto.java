package com.spring.springboot.dto;

import lombok.Data;
import lombok.ToString;


@Data
@ToString
public class MyUserDto {

    private Long id;
    private String name;
    private String password;
    private String role;

}
