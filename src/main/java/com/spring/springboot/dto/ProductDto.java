package com.spring.springboot.dto;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class ProductDto {

    private Long id;
    private String name;
    private double cost;

}
