package com.spring.springboot.mapper;

import com.spring.springboot.dto.ProductDto;
import com.spring.springboot.entity.Product;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ProductMapper {

    Product toEntity(ProductDto dto);

    ProductDto toDto(Product entity);

    List<Product> toEntity(List<ProductDto> dtos);

    List<ProductDto> toDto(List<Product> entities);

}
