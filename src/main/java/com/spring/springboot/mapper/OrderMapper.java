package com.spring.springboot.mapper;

import com.spring.springboot.dto.OrderDto;
import com.spring.springboot.entity.Order;
import org.mapstruct.Mapper;
import java.util.List;

@Mapper(componentModel = "spring")
public interface OrderMapper {

    Order toEntity(OrderDto dto);

    OrderDto toDto(Order entity);

    List<Order> toEntity(List<OrderDto> dtos);

    List<OrderDto> toDto(List<Order> entities);

}
