package com.spring.springboot.mapper;

import com.spring.springboot.dto.MyUserDto;
import com.spring.springboot.entity.auth.MyUser;
import org.mapstruct.Mapper;

import java.util.List;


@Mapper(componentModel = "spring")
public interface MyUserMapper {

    MyUser toEntity(MyUserDto dto);

    MyUserDto toDto(MyUser entity);

    List<MyUser> toEntity(List<MyUserDto> dtos);

    List<MyUserDto> toDto(List<MyUser> entities);

}
