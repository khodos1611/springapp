package com.spring.springboot.security;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
@ComponentScan("com.spring.springboot")
public class SecurityConfig {

    @Bean
    public SecurityFilterChain getFilterChain(HttpSecurity httpSecurity) throws Exception {

        return  httpSecurity.csrf().disable()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers("/ping/**").permitAll()
                .antMatchers(HttpMethod.POST,"/**").hasRole("Admin")
                .antMatchers(HttpMethod.DELETE,"/**").hasRole("Admin")
                .anyRequest()
                .authenticated()
                .and()
                .httpBasic()
                .and()
                .build();
    }


}
